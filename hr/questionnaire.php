<!DOCTYPE html>
<html lang="hr">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../pepiso.css">
<link rel="icon" type="image/png"
  href="http://www.pepiso-management.hr/img/icons/favicon.png">
<title>Pepiso Management - Upitnik</title>

<style type="text/css">
body, h1, p {
  font-family: "Helvetica Neue", "Segoe UI", Segoe, Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: normal;
  margin: 0;
  padding: 0;
  text-align: center;
}

body {
	background: white;
	}

header {
  margin: 2px;
  padding-top: 50px;
  height: 350px;
  background: #272262
}

header img {
  display: block;
  margin: 35px auto;
}

.social {
  width: 50px;
  height: 50px;
  background: white;
  color: grey;
}

nav a {
  display: inline-block;
  width: 100%;
  background: grey;
  color: white;
  height: 50px;
  line-height: 50px;
  margin: 1px;
}

.container {
  margin-left:  auto;
  margin-right:  auto;
  margin-top: 177px;
  max-width: 1170px;
  padding-right: 15px;
  padding-left: 15px;
}

.row:before, .row:after {
  display: table;
  content: " ";
}

h1 {
  font-size: 48px;
  font-weight: 300;
  margin: 0 0 20px 0;
}

.lead {
  font-size: 21px;
  font-weight: 200;
  margin-bottom: 20px;
}

p {
  margin: 0 0 10px;
}

a {
  color: #3282e6;
  text-decoration: none;
}

footer {
  background: #272262; 
  height: 350px;
  color: white;
  margin: 2px;
}

footer div {
  display: inline-block;
  width: 32%;
}

#about h3 {
  text-align: center;
  color: white;
  background: #272262; 
}

#contact-info {
  text-align: left;
}

#useful {
  text-align: left;
}
#useful a {
  display: block;
  background: gray;
  text-transform: uppercase;
  width: 350px;
  color: white;
  margin: 1px;
  font-weight: bold;
}

	</style>
</head>

<body>
<header>
  <a href="">HR</a> / <a href="">ENG</a>
  <img alt="Pepiso Management j.d.o.o." src="img/logo.jpg" width="440">
  <a class="social" href="">instagram</a>
  <a class="social" href="">facebook</a>
  <a class="social" href="">whatsapp</a>
  <a class="social" href="">youtube</a>
</header>

<nav>
<a href="">odmor po mjeri</a>
<a href="">izleti</a>
<a href="">najam vozila</a>
<a href="">upravljanje u iznajmljivanju smještaja</a>
<a href="">kontakt</a>
<a href="">o nama</a>
</nav>

<!-- weather widget -->
<a class="weatherwidget-io" href="https://forecast7.com/en/43d3316d45/milna/" data-label_1="MILNA" data-label_2="WEATHER" data-theme="original" >MILNA WEATHER</a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>

<div id="about">
<h3>o nama</h3>
text text text 
</div>

<footer>
	<h3>kontakt</h3>
	
<div id="contact-info">
Address:<br>
Vlaška 1/8<br>
21405 Milna<br>
Republika Hrvatska<br>
E-mail: pepiso.management@gmail.com
</div>

<div>form</div>

<div id="useful">
	Useful links:
	<a href="#">&bull; emergency services contacts</a>
	<a href="#">&bull; customs introduction</a>
	<a href="#">&bull; regulatory instructions</a>
	<a href="#">&bull; traffic conditions</a>
	<a href="#">&bull; currency exchange rate</a>
	<a href="#">&bull; tourist office milna</a>
</div>
</footer>
</body>
</html>
